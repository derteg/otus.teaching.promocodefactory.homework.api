﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalRHubs
{
    public class CustomerHub : Hub
    {
        private readonly ILogger<CustomerHub> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerHub(ILogger<CustomerHub> logger, IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return response;
        }

        public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
          
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            var connectionId = Context.ConnectionId;

            await Clients.All.SendAsync("Notify", connectionId, $"создал customer'а c Id: {customer.Id} ");
            return await GetCustomerAsync(customer.Id);
        }

        public async Task<string> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return "Not Found Customer";

            await _customerRepository.DeleteAsync(customer);
            var connectionId = Context.ConnectionId;

            await Clients.All.SendAsync("Notify", connectionId, $"удалил customer'а c Id: {id} ");

            return $"Deleted customer with Guid: {id}";
        }


        public async Task<string> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return "Not Found Customer";

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            
            var connectionId = Context.ConnectionId;

            await Clients.All.SendAsync("Notify", connectionId, $"изменил customer'а c Id: {id} ");

            return "Success";
        }
    }
}
