﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly ILogger<CustomersService> _logger;

        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;
        //private readonly ApplicationContext _context;

        public CustomersService(ILogger<CustomersService> logger, IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)//, ApplicationContext context)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            // _context = context;
        }

        public override async Task<CustomersShortResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerShortResponse = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var response = new CustomersShortResponse();
            response.Customers.AddRange(customerShortResponse);

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(IdRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Wrong Id"));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer Not Found"));

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
            response.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }).ToHashSet());

            return response;
        }

        public override async Task<Empty> DeleteCustomer(IdRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Wrong Id"));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer Not Found"));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());

            var customer = new Customer()
            {
                Id = string.IsNullOrEmpty(request.Id) ? Guid.NewGuid() : Guid.Parse(request.Id),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id) == false)
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Wrong Id"));

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer Not Found"));

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;


            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            await _customerRepository.UpdateAsync(customer);
            return new Empty();

        }
    }
}
