﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcClient.Protos;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);
            Console.WriteLine("Customers GRPC ");
            Console.WriteLine("----------------------------------------------------------\n");
           

            //Get
            Console.WriteLine("GET ALL CUSTOMERS");
            var customers = await client.GetCustomersAsync(new Google.Protobuf.WellKnownTypes.Empty());
            foreach (var cm in customers.Customers)
            {
                Console.WriteLine($"{cm.Id} {cm.FirstName} {cm.LastName} {cm.Email}");
            }


            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("CREATE CUSTOMER WITH ID: F37A19C6-D775-4624-B978-8C5650372AEF");
            //Put
            await client.CreateCustomerAsync(new CreateOrEditCustomerRequest()
            {
                Email = "grpc@mail.ry",
                FirstName = "grpcFN",
                LastName = "grpcLN",
                Id = "F37A19C6-D775-4624-B978-8C5650372AEF",
                PreferenceIds =
                {
                    "c4bda62e-fc74-4256-a956-4760b3858cbd",//семья
                    "76324c47-68d2-472d-abb8-33cfa8cc0c84",//дети
                }
            });
            Console.WriteLine("OK\n");
            
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("GET CUSTOMER BY ID F37A19C6-D775-4624-B978-8C5650372AEF\n");
            
            //get by Id with pref
            var customer = await client.GetCustomerAsync(new IdRequest
            {
                Id = "F37A19C6-D775-4624-B978-8C5650372AEF"
            });
            Console.WriteLine($"{customer.Id} {customer.FirstName} {customer.LastName} {customer.Email}");
            foreach (var pref in customer.Preferences)
            {
                Console.WriteLine($"{pref.Id} {pref.Name}");
            }
            Console.WriteLine("OK");
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("EDID CUSTOMER BY ID F37A19C6-D775-4624-B978-8C5650372AEF");

            //get by Id with pref
            await client.EditCustomerAsync(new CreateOrEditCustomerRequest()
            {
                Id = "F37A19C6-D775-4624-B978-8C5650372AEF",
                Email = "edit@mail.ry",
                FirstName = "editFN",
                LastName = "editLN",
                //PreferenceIds =
                //{ 
                //    "c4bda62e-fc74-4256-a956-4760b3858cbd",//семья
                //}
            });
            Console.WriteLine("OK");
            Console.WriteLine("CHECK EDIT CUSTOMER BY ID F37A19C6-D775-4624-B978-8C5650372AEF\n");
            
            var editCustomerCheck = await client.GetCustomerAsync(new IdRequest
            {
                Id = "F37A19C6-D775-4624-B978-8C5650372AEF"
            });
            Console.WriteLine($"{editCustomerCheck.Id} {editCustomerCheck.FirstName} {editCustomerCheck.LastName} {editCustomerCheck.Email}");
            foreach (var pref in editCustomerCheck.Preferences)
            {
                Console.WriteLine($"{pref.Id} {pref.Name}");
            }
            Console.WriteLine("OK\n");
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("DELETE CUSTOMER BY ID F37A19C6-D775-4624-B978-8C5650372AEF");

            //get by Id with pref
            await client.DeleteCustomerAsync(new IdRequest
            {
                Id = "F37A19C6-D775-4624-B978-8C5650372AEF"
            });
            Console.WriteLine("OK");
            
            Console.WriteLine("CHECK DELETE CUSTOMER BY ID F37A19C6-D775-4624-B978-8C5650372AEF");
            Console.WriteLine("ERROR EXPEСTED\n");
            

            try
            {
                await client.GetCustomerAsync(new IdRequest
                {
                    Id = "F37A19C6-D775-4624-B978-8C5650372AEF"
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
               
            }
            
            Console.ReadKey();

        }
    }
}
